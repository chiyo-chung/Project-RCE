package remoteexecutor

import (
	"fmt"
	"golang.org/x/crypto/ssh"
	"net"
	"time"
)

type RCE struct {
	IP         string
	Username   string
	Password   string
	Port       int
	client     *ssh.Client
	LastResult string
}

func New(IP string, Username string, Password string, Port int) *RCE {
	RCE := new(RCE)
	RCE.IP = IP
	RCE.Username = Username
	RCE.Password = Password
	RCE.Port = Port
	return RCE
}


func (c RCE) Run(cmd string) (string, error) {
	session, err := c.client.NewSession()
	defer session.Close()

	if err != nil {
		return "", err
	}

	buf, err := session.CombinedOutput(cmd)
	c.LastResult = string(buf)
	return c.LastResult, err
}

func (c *RCE) Connect() error {
	config := ssh.ClientConfig{
		User: c.Username,
		Auth: []ssh.AuthMethod{ssh.Password(c.Password)},
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},
		Timeout: 10 * time.Second,
	}
	addr := fmt.Sprintf("%s:%d", c.IP, c.Port)
	sshClient, err := ssh.Dial("tcp", addr, &config)
	if err != nil {
		return err
	}
	c.client = sshClient
	return nil
}


